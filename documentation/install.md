---
title: Installation Guide
description: Install GlitchTip on your own server with Docker Compose, DigitalOcean, Heroku, or Helm.
publish: true
---

# GlitchTip Installation Guide

GlitchTip can be run with Docker. We recommend [Docker Compose](documentation/install#docker-compose), [DigitalOcean App Platform](documentation/install#digitalocean-app-platform), or [Heroku](documentation/install#heroku) for smaller installations. A [Helm](documentation/install#helm) chart is available for Kubernetes.

### System Requirements

GlitchTip requires Postgresql (11+), Redis, a web service, and a worker service.

- Recommended system requirements: 1GB ram
- Minimum system requirements: 512MB ram + swap

Disk usage varies on usage and event size. As a rough guide, a 1 million event per month instance may require 30GB of disk.

## [Docker Compose](documentation/install#docker-compose)

Docker Compose is a simple way to run GlitchTip on a single server.

1. Install Docker and Docker Compose. On Debian/Ubuntu this is `sudo apt install docker-compose docker.io`
2. Copy [docker-compose.sample.yml](/assets/docker-compose.sample.yml) to your server as `docker-compose.yml`.
3. Edit the environment section of docker-compose.yml. See the Configuration section below.
4. Start docker service `docker-compose up -d` now the service should be running on port 8000.

It's highly recommended to configure SSL next. Use nginx or preferred solution.

### [Recommended nginx and SSL solution](documentation/install#recommended-nginx-and-ssl-solution)

- Install nginx. Ex: `sudo apt install nginx`.
- (on Debian/Ubuntu) edit `/etc/nginx/sites-enabled/default` for example:

```
server {
    server_name glitchtip.example.com;
    access_log  /var/log/nginx/access.log;

    location / {
        proxy_pass http://127.0.0.1:8000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
```

This configuration will direct glitchtip.example.com to port 8000 (the default GlitchTip docker compose port).

- Install and run certbot. Follow [instructions](https://certbot.eff.org/instructions).

### [Upgrading](documentation/install#upgrading)

1. Pull latest docker image `docker-compose pull`
1. Restart `docker-compose stop` and `docker-compose up -d`

Database migrations will automatically happen.

## [DigitalOcean App Platform](documentation/install#digitalocean-app-platform)

Get started by clicking here. Note this is a referral link and is a great way to help fund GlitchTip.

<a href="https://cloud.digitalocean.com/apps/new?repo=https://gitlab.com/glitchtip/glitchtip/tree/master&refcode=7e90b8fb37f8">
<img src="https://www.deploytodo.com/do-btn-blue.svg" alt="Deploy to DigitalOcean" style="width:250px;"/>
</a>

Leave environment variables blank and click next. Pick the basic or pro plan. One 512 MB RAM | 1 vCPU is fine to start with. Click Launch. Now copy [app-platform.yaml](https://gitlab.com/glitchtip/glitchtip/-/blob/master/app-platform.yaml) to your local computer. Edit the following

### [Name and region](documentation/install#name-and-region)

This can be anything. We default to "glitchtip" and "nyc".

### [Environment Variables](documentation/install#environment-variables)

At a minimum, set the SECRET_KEY to a random string of letters.

See [Configuration](https://glitchtip.com/documentation/install#configuration) for more information.

### [Redis](documentation/install#redis)

GlitchTip requires Redis for sending notification, managing events, and more. Go to https://cloud.digitalocean.com/databases/ and create a new redis database. For almost all size instances, the 1 GB RAM | 1 vCPU instance is sufficient. Enter your redis database's name in the glitchtip-redis section. Let's assume it's named "glitchtip-redis". Both "name" and "cluster_name" must be the same value.

```
- name: glitchtip-redis
  engine: REDIS
  production: true
  cluster_name: glitchtip-redis
```

Ensure the environment variable "REDIS_URL" uses the same name. If you didn't name your redis instance "glitchtip-redis" then make sure to update it.

### [Deploying](documentation/install#deploying)

You'll need to install [doctl](https://www.digitalocean.com/docs/apis-clis/doctl/how-to/install/) and log in.

Run `doctl apps list` to get your app's id.

Now apply your app-platform.yaml spec with `doctl apps update 11111111-1111-1111-1111-111111111 --spec app-platform.yaml` (enter your actual id)

After deployment, you should be able to visit the app URL and start using GlitchTip!

### [Production considerations](documentation/install#production-considerations)

If you intend to use GlitchTip in production, consider upgrading your Postgres database to a production instance. In the web interface, go to Manage Components, glitchtip-db, Upgrade to a managed database.

If you haven't already, you'll need to set up email via environment variables.

### [Upgrading GlitchTip](documentation/install#upgrading-glitchtip)

By default, the docker image tag is "latest". Click Deploy to upgrade to the latest GlitchTip docker image.

## [Heroku](documentation/install#heroku)

<a href="https://heroku.com/deploy?template=https://github.com/burke-software/GlitchTip">
<img src="https://www.herokucdn.com/deploy/button.svg" alt="Deploy to Heroku" style="width:200px;"/>
</a>

1. Deploy the app by clicking the above link and following the prompt.
2. SECRET_KEY is generated for you. Other environment variables must be entered in the app's Settings, Config Vars. See configuration [docs](https://glitchtip.com/documentation/install#configuration).

### [Production considerations](documentation/install#production-considerations-1)

Consider upgrading your Postgres and web dyno plan for production usage.

Most users do not need additional workers. However if you do, create a third dyno typed called extra_worker. Set the run command to `./bin/run-celery.sh`. Do not increase the "worker" dyno count because this these run with an embedded Celery beat scheduler.

### [Upgrading GlitchTip](documentation/install#upgrading-glitchtip-1)

By default, the docker image tag is "latest". Click Deploy to upgrade to the latest GlitchTip docker image.

## [Helm](documentation/install#helm)

Installing GlitchTip with Helm for Kubernetes is a good option for high throughput sites and users who are very comfortable using Kubernetes.

app.glitchtip.com uses this method with a managed DigitalOcean database.

1. Add our Helm chart repo `helm repo add glitchtip https://gitlab.com/api/v4/projects/16325141/packages/helm/stable`
2. Review our [values.yaml](https://gitlab.com/glitchtip/glitchtip-helm-chart/-/blob/master/values.yaml) and [values.sample.yaml](https://gitlab.com/glitchtip/glitchtip-helm-chart/-/blob/master/values.sample.yaml). At a minimum, decide if using helm postgresql and set env.secret.SECRET_KEY
3. Install the chart `helm install glitchtip glitchtip/glitchtip -f your-values.yaml`. You'll need to specify your own values.yml file or make use of `--set`.

For postgresql, we recommend an externally managed database and providing only the DATABASE_URL environment variable. If using helm managed postgresql, then make sure to consider:

- If you uninstall the chart, it will not delete the pvc. If you reinstall the chart, it won't have the correct password because of this.
- postgresql helm chart does not support major upgrades (such as 14.0 to 15.0). It will fail to start. You could export to a sql file and import if downtime is acceptable. Minor updates are supported.

For high availability, production servers we recommend using multiple Kubernetes Nodes, an ingress and/or load balancer, a pod disruption budget, anti-affinity, and a managed PostgreSQL high availability database.

# [Configuration](documentation/install#configuration)

Required environment variables:

- `SECRET_KEY` set to any random string
- Set up email:
- `EMAIL_URL`: SMTP string. It will look something like `"smtp://email:password@smtp_url:port"`. See format examples [here](https://django-environ.readthedocs.io/en/latest/#supported-types).
- Alternatively, use the Mailgun API by setting `MAILGUN_API_KEY` and `MAILGUN_SENDER_DOMAIN`. Set `EMAIL_BACKEND` to `anymail.backends.mailgun.EmailBackend`
- Alternatively, use the SendGrid API by setting `SENDGRID_API_KEY`. Set `EMAIL_BACKEND` to `anymail.backends.sendgrid.EmailBackend`.
- `DEFAULT_FROM_EMAIL` Default from email address. Example `info@example.com`
- `GLITCHTIP_DOMAIN` Set to your domain. Include scheme (http or https). Example: `https://glitchtip.example.com`.

Optional environment variables:

- `I_PAID_FOR_GLITCHTIP` [Donate](https://liberapay.com/GlitchTip/donate), set this to "true", and some neat things will happen. This won't enable extra features but it will enable our team to continue building GlitchTip. We pay programmers, designers, illustrators, and free tier hosting on app.glitchtip.com without venture capital. We ask that all self-host users pitch in with a suggested donation of $5 per month per user. Prefer an invoice and support instead? Business users can also consider a paid support plan. Reach out to us at sales@glitchtip.com. Contributors on [Gitlab](https://gitlab.com/glitchtip) should also enable this.
- `GLITCHTIP_MAX_EVENT_LIFE_DAYS` (Default 90) Events and associated data older than this will be deleted from the database
- `REDIS_URL` Set redis host explicitly. Example: `redis://:password@host:port/database`. You may also set them separately with `REDIS_HOST`, `REDIS_PORT`, `REDIS_DATABASE`, and `REDIS_PASSWORD`.
- `DATABASE_URL` Set PostgreSQL connect string. PostgreSQL 11 and above are supported.
- Content Security Policy (CSP) headers are enabled by default. In most cases there is no need to change these. However you may add environment variables as documented in [django-csp](https://django-csp.readthedocs.io/en/latest/configuration.html#policy-settings) to modify them. For example, set `CSP_DEFAULT_SRC='self',scripts.example.com` to modify the default CSP header. Note the usage of comma separated values and single quotes on certain values such as 'self'.
- `ENABLE_OPEN_USER_REGISTRATION` (Default False) Set to True to allow any user to register. When False, user self sign up is disabled after the first organization is created. This setting may be useful for SaaS services or self hosting that encourage peers to sign themselves up.

### [File storage](documentation/install#file-storage)

Storage is necessary to enable file uploads, such as sourcemaps. GlitchTip can support both local storage and remote storage via [django-storages](https://django-storages.readthedocs.io/en/latest/).

GlitchTip maps environment variables to django-storages configuration. If you find that your configuration option is supported by django-storages but not GlitchTip, please submit a [merge request](https://gitlab.com/glitchtip/glitchtip-backend/-/merge_requests).

#### AWS S3 or DigitalOcean Spaces

[django-storages S3 documentation](https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html)

- DEFAULT_FILE_STORAGE - `storages.backends.s3boto3.S3Boto3Storage`
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_STORAGE_BUCKET_NAME
- AWS_S3_ENDPOINT_URL - Necessary if using DigitalOcean Spaces. Set to `https://<your-region>.digitaloceanspaces.com`

#### Azure Blob Storage

[django-storages Azure documentation](https://django-storages.readthedocs.io/en/latest/backends/azure.html)

- DEFAULT_FILE_STORAGE - `storages.backends.azure_storage.AzureStorage`
- AZURE_ACCOUNT_NAME
- AZURE_ACCOUNT_KEY
- AZURE_CONTAINER
- AZURE_URL_EXPIRATION_SECS - Set if not using public ACL

#### Google Cloud Storage

[django-storages Google Cloud documentation](https://django-storages.readthedocs.io/en/latest/backends/gcloud.html)

- DEFAULT_FILE_STORAGE - `storages.backends.gcloud.GoogleCloudStorage`
- GS_BUCKET_NAME
- GS_PROJECT_ID
- GOOGLE_APPLICATION_CREDENTIALS

#### Local Docker Volume

For local storage with Docker, use a volume. Refer to Kubernetes or Docker Compose documentation on creating volumes. In the future, docker-compose examples with volumes will be provided by default.

### [Search Language](documentation/install#search-language)

GlitchTip uses PostgreSQL full-text search. It will use the default PostgreSQL "text_search_config". In most cases there is no need to modify this. However, you may wish to change it as described [here](https://www.postgresql.org/docs/13/textsearch-configuration.html). This only affects search terms, it does not affect the site language. For example, if your preferred reading language is French and your code and user base uses English, you should pick English.

## [Django Admin](documentation/install#django-admin)

Django Admin is not necessary for most users. However if you'd like the ability to fully manage users beyond what our frontend offers, it may be useful. To enable, create a super user via the Django command

`./manage.py createsuperuser`

Then go to `/admin/` and log in.

### [Social Authentication (OAuth)](documentation/install#social-authentication-oauth)

You may add Social Accounts in Django Admin at `/admin/socialaccount/socialapp/`. GlitchTip supports the following providers though django-allauth:

- [DigitalOcean](https://django-allauth.readthedocs.io/en/latest/providers.html#digitalocean)
- [Gitea](https://django-allauth.readthedocs.io/en/latest/providers.html#gitea)
- [Github](https://django-allauth.readthedocs.io/en/latest/providers.html#github)
- [Gitlab](https://django-allauth.readthedocs.io/en/latest/providers.html#gitlab)
- [Google](https://django-allauth.readthedocs.io/en/latest/providers.html#google)
- [Microsoft](https://django-allauth.readthedocs.io/en/latest/providers.html#microsoft-graph)
- [NextCloud](https://django-allauth.readthedocs.io/en/latest/providers.html#nextcloud)
- [Keycloak](https://django-allauth.readthedocs.io/en/latest/providers.html#keycloak)

# Custom domain and settings

It's possible to edit django-allauth settings via environment variables. The following are supported.

- SOCIALACCOUNT_PROVIDERS_gitlab_GITLAB_URL
- SOCIALACCOUNT_PROVIDERS_gitea_GITEA_URL
- SOCIALACCOUNT_PROVIDERS_nextcloud_SERVER
- SOCIALACCOUNT_PROVIDERS_keycloak_KEYCLOAK_URL
- SOCIALACCOUNT_PROVIDERS_keycloak_KEYCLOAK_URL_ALT
- SOCIALACCOUNT_PROVIDERS_keycloak_KEYCLOAK_REALM

For more infomation, refer to django-allauth's [providers documentation](https://django-allauth.readthedocs.io/en/latest/providers.html). If your OAuth provider or a needed configuration option is supported by django-allauth but not GlitchTip, please open a merge request to add support.
