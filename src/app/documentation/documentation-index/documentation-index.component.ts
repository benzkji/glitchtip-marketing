import { Component } from '@angular/core';
import { LinksService } from 'src/app/links.service';

@Component({
  selector: 'app-documentation-index',
  templateUrl: './documentation-index.component.html',
  styleUrls: ['./documentation-index.component.scss'],
})
export class DocumentationIndexComponent {
  registerLink = this.links.registerLink;

  constructor(private links: LinksService) {}
}
