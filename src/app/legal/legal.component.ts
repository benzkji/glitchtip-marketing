import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-legal',
  templateUrl: './legal.component.html',
  preserveWhitespaces: true,
  encapsulation: ViewEncapsulation.Emulated,
})
export class LegalComponent {
  constructor() {}
}
