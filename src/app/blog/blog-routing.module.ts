import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlogComponent } from './blog.component';
import { BlogIndexComponent } from './blog-index/blog-index.component';

const routes: Routes = [
  {
    path: '',
    component: BlogIndexComponent
  },
  {
    path: ':slug',
    component: BlogComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule {}
